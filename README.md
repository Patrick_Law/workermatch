# Worker Matcher #

A REST API that takes a workerId and return no more than three appropriate jobs for that Worker. 

### What is this repository for? ###

* Programming brief for swipejobs.

### How do I get set up? ###

* A Maven Webapp which uses Spring framework and Tomcat Apache.
* Import to Eclipse project and run index.jsp
