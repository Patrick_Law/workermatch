package com.matcher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.matcher.service.MatchService;

@Controller
public class AddController {
	
	@RequestMapping("/match")
	public ModelAndView match(HttpServletRequest request, HttpServletResponse response) {
		
		int userID = Integer.parseInt(request.getParameter("workerID"));
		
		MatchService ms = new MatchService();
		String myResult = ms.match(userID);
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("display.jsp");
		mv.addObject("result",myResult);
		
		return mv;
	}

}
