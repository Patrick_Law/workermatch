package com.matcher.service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * Input workedId to return up to 3 best job matches 
 * Based on certificates, skills, hasDriversLicense, jobSearchAddress.
 * 
 * @param workerId
 * @return
 */
public class MatchService {
	public String match(int workerID) {
		
		//Read workers db.
		RestTemplate restTemplateWorker = new RestTemplate();
		ResponseEntity<List<Worker>> workerResponse = restTemplateWorker.exchange("http://test.swipejobs.com/api/workers",
		                    HttpMethod.GET, null, new ParameterizedTypeReference<List<Worker>>() {
		            });
		List<Worker> worker = workerResponse.getBody();
		
		//Search workers db for your requested ID.
		List<Worker> myWorker = worker.stream().filter(item -> item.getUserId().equals(workerID)).collect(Collectors.toList());

		//End early if worker not found.
		if (myWorker.isEmpty()) {
			return "Worker not found, please enter a valid worker ID.";
		}
		
		//Read jobs db.
		RestTemplate restTemplateJobs = new RestTemplate();
		ResponseEntity<List<Job>> jobResponse = restTemplateJobs.exchange("http://test.swipejobs.com/api/jobs",
		                    HttpMethod.GET, null, new ParameterizedTypeReference<List<Job>>() {
		            });
		List<Job> job = jobResponse.getBody();

		//Search jobs db with your worker's info.
		
		//Filter out jobs with mandatory requirements: License, Certificates, Skills.
		List<Job> driversLicenseFilter = job.stream().filter(item -> item.getDriverLicenseRequired().equals(myWorker.get(0).getHasDriversLicense())).collect(Collectors.toList());
		List<Job> certificatesFilter = driversLicenseFilter.stream().filter(item -> myWorker.get(0).getCertificates().containsAll(item.getRequiredCertificates())).collect(Collectors.toList());
		List<Job> skillsFilter = certificatesFilter.stream().filter(item -> myWorker.get(0).getSkills().contains(item.getJobTitle())).collect(Collectors.toList());
		List<Job> distanceFilter = skillsFilter.stream().filter(item -> haversine(myWorker.get(0), item)).collect(Collectors.toList());
		
		//Sort by pay descending grade and limit to 3 result.
		List<Job> limitTo3Results = distanceFilter.stream().sorted(payComparator().reversed()).limit(3).collect(Collectors.toList());
		
		if (limitTo3Results.isEmpty()) {
			return "No appropriate jobs currently available.";
		}
		
		return "Appropriate jobs for worker ID " + workerID + " " + myWorker.toString()  + ":<br/>" + limitTo3Results.toString();
	}

	private static Comparator<Job> payComparator() {
		Comparator<Job> comparator = Comparator.comparing(Job::getBillRate);
		return comparator;
	}

	public static boolean haversine(Worker worker, Job job) {
		//Modified version of https://stackoverflow.com/questions/18861728/calculating-distance-between-two-points-represented-by-lat-long-upto-15-feet-acc
		double lat1 = Double.parseDouble(worker.getJobSearchAddress().getLatitude());
		double lng1 = Double.parseDouble(worker.getJobSearchAddress().getLongitude());
		double lat2 = Double.parseDouble(job.getLocation().getLatitude());
		double lng2 = Double.parseDouble(job.getLocation().getLongitude());
		int r = 6371; // average radius of the earth in km
		double dLat = Math.toRadians(lat2 - lat1);
		double dLon = Math.toRadians(lng2 - lng1);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double d = r * c;
		
		// return true if within maxJobDistance
		if (d <= worker.getJobSearchAddress().getMaxJobDistance()) {
			return true;
		} else {
			return false;
		}
	}
}
